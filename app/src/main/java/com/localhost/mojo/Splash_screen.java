package com.localhost.mojo;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.localhost.mojo.util.AlertDialogFragment;
import com.localhost.mojo.util.Gps_utility;

import com.google.android.gms.location.LocationServices;

public class Splash_screen extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private GoogleApiClient mGoogleApiClient = null;
    Gps_utility mgps_utility;
    AlertDialog alert;
    private final String TAG = "Splash Screen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mgps_utility= new Gps_utility(this);
        if (!mgps_utility.is_gps_enabled()) {
            show_gps_dialog();
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .build();
        //mGoogleApiClient = GoogleApiClientSingleton.getGoogleApiClient();
        mGoogleApiClient.registerConnectionFailedListener(this);
        mGoogleApiClient.registerConnectionCallbacks(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(alert != null) {
            alert.dismiss();
        }
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(alert != null) {
            alert.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(alert != null) {
            alert.dismiss();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(!mgps_utility.is_gps_enabled()) {
            if(alert != null) {
                alert.dismiss();
            }
            show_gps_dialog();
        }
    }

    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("onConnectionFailed","called");

        Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        //TODO(Developer): Check error code and notify the user of error state and resolution.
        showAlertDialog(R.string.connection_alert_title);

    }
    @Override
    public void onConnectionSuspended(int arg) {
        Log.e("onConnectionSuspended", "called");

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if(mGoogleApiClient.isConnected()) {
            if(mgps_utility.is_gps_enabled()) {
                Intent intent = new Intent(this,Container.class);
                if(alert!= null) {
                    alert.dismiss();
                }
                startActivity(intent);
            } else {
                if(alert!= null) {
                    alert.dismiss();
                }
                show_gps_dialog();
            }
        }
        else {
            showAlertDialog(R.string.connection_alert_title);
        }
    }

    public void doPositiveClick() {
        Log.e("doPositiveClick","called");

    }

    void showAlertDialog(int title) {
        Log.e("showAlertDialog","called");
        AlertDialogFragment newFragment = AlertDialogFragment.newInstance(title);
        newFragment.show(getFragmentManager(), "dialog");
    }

    public void show_gps_dialog() {
        AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Improve Location Accuracy")
                .setMessage("This app requires gps to be enabled. Please turn on the gps.")
                .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mgps_utility.enable_gps();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alert = builder.create();
        alert.show();
    }
}

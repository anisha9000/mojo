package com.localhost.mojo.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.localhost.mojo.Splash_screen;

/**
 * Created by anisha on 10/8/15.
 */
public class AlertDialogFragment extends DialogFragment {

    //private static Context context;
    public static AlertDialogFragment newInstance(int title) {

        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();

        args.putInt("title", title);
        //  context = activityContext;
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int title = getArguments().getInt("title");

        return new AlertDialog.Builder(getActivity())
                //  .setIcon(R.drawable.alert_dialog_icon)
                .setTitle(title)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (getActivity().getClass().equals(Splash_screen.class)) {
                                    ((Splash_screen) getActivity()).doPositiveClick();
                                }
                            }
                        }
                )
                .create();
    }
}

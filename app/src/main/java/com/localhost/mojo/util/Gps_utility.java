package com.localhost.mojo.util;

import android.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.localhost.mojo.Splash_screen;

/**
 * Created by anisha on 9/8/15.
 */
public class Gps_utility {

    Context context;
    LocationManager manager;
    boolean isGPSEnabled;

    public Gps_utility(Context context) {
        this.context = context;
    }

    public boolean is_gps_enabled() {
        manager = (LocationManager) context.getSystemService( context.LOCATION_SERVICE );
        isGPSEnabled = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(isGPSEnabled) {
            return true;
        } else {
            return false;
        }
    }

    public void enable_gps() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(intent);
    }

    public void show_gps_dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Improve Location Accuracy")
                .setMessage("This app requires gps to be enabled. Please turn on the gps.")
                .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        enable_gps();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity)context).finish();
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}

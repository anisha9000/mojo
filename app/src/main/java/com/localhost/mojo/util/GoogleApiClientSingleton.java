package com.localhost.mojo.util;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
/**
 * Created by anisha on 9/8/15.
 */
public class GoogleApiClientSingleton extends Application {
    private static GoogleApiClient mGoogleApiClient;
    public Context mContext;
    private final static String TAG = "GoogleApiClientSingleton";

    @Override
    public void onCreate()
    {
        //super.onCreate();
        mContext = getApplicationContext();

        try {
            Log.e("Singleton"," caling builder");
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .build();
            if(mGoogleApiClient != null) {
                Log.e("Singleton"," builder success");
            } else {
                Log.e("Singleton"," builder failed");
            }
        } catch (Exception e) {
            Log.e("GoogleApiSingleton", e.toString());
        }
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();
    }

    public static GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }


}
